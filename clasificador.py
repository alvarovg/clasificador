#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    if "@" in string:
        separar = string.split("@")
        if separar[0] != "" and "." in separar[1]:
            return True
        else:
            return False
    else:
        return False

def es_entero(shtring):
    try:
        int (shtring)
        return True
    except:
        return False

def es_real(entrada):
    if entrada.count(".") == 1:
        entero_decimal = entrada.split(".")
        try:
            int(entero_decimal[0]) and int(entero_decimal[1])
            return True
        except:
            return False
    else:
        return False
es_real("a.25")
def evaluar_entrada(string):
    if string == "":
        respuesta = None
    elif es_correo_electronico(string):
        respuesta = "Es un correo electrónico."
    elif es_entero(string):
        respuesta = "Es un entero."
    elif es_real(string):
        respuesta = "Es un número real."
    else:
        respuesta = "No es ni un correo, ni un entero, ni un número real."
    return respuesta

def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
